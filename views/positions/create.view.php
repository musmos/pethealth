<?php require_once('../../views/layouts/master.view.php')?>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        Add Position
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Add Position</h5>
                        <div class="row">
                            <div class="col-sm-6">
                                <form action="../../controllers/positions/store.php" method="POST">
                                    <div class="form-group">
                                        <label for="">Name:</label>
                                        <input type="text" class="form-control" name='name' required>
                                        <br>
                                        <input type="submit" class="btn btn-primary">
                                    </div>
                                </form>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require_once('../../views/layouts/footer.view.php')?>