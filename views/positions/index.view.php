<?php require_once('../../views/layouts/master.view.php')?>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header" style="display: flex; justify-content: space-between;">
                        Positions
                        <a href="../../controllers/positions/create.php" class="btn btn-primary">Add Position</a>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Positions</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>	
<?php require_once('../../views/layouts/footer.view.php')?>