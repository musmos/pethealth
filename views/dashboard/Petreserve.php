<?php
    session_start();
    require('../../core/functions.php');
    $pdo = connectToDB();

    $statement = $pdo->prepare('SELECT * FROM products');
    $statement->execute();
    $products = $statement->fetchAll(PDO::FETCH_OBJ);

    $stmnt = $pdo->prepare('SELECT * FROM schedule');
    $stmnt->execute();
    $sched = $stmnt->fetchAll(PDO::FETCH_OBJ);

    $stmnts = $pdo->prepare('SELECT * FROM schedule');
    $stmnts->execute();
    $scheds = $stmnts->fetch(PDO::FETCH_OBJ);

    // var_dump($scheds->id);   
    // die();
     
?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>PetHealth.com | Wis</title>
    <meta charset="UTF-8">
    <meta name="description" content=" PetHealth.com | Technopreneurship">
    <meta name="keywords" content="PetHealth.com, eCommerce, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link href="img/favicon.ico" rel="shortcut icon"/>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css"/>
    <link rel="stylesheet" href="../../css/font-awesome.min.css"/>
    <link rel="stylesheet" href="../../css/flaticon.css"/>
    <link rel="stylesheet" href="../../css/slicknav.min.css"/>
    <link rel="stylesheet" href="../../css/jquery-ui.min.css"/>
    <link rel="stylesheet" href="../../css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="../../css/animate.css"/>
    <link rel="stylesheet" href="../../css/style.css"/>




    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header section -->
    <header class="header-section">
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 text-center text-lg-left">
                        <!-- logo -->
                        <a href="./index.view.php" class="site-logo">
                            <img src="../../img/logo.png" alt="">
                        </a>
                    </div>
                    <div class="col-xl-6 col-lg-5">
                        <form class="header-search-form">
                            <input type="text" placeholder="Search on Pethealth.com ....">
                            <button><i class="flaticon-search"></i></button>
                        </form>
                    </div>
                    <div class="col-xl-4 col-lg-5">
                            <div class="user-panel">
                                <div class="up-item">
                                    <i class="flaticon-profile"></i>
                                    <a href="#"><?php echo $_SESSION['username'] ?></a>
                                </div>
                                <div class="up-item">
                                    <div class="shopping-card">
                                        <i class="flaticon-bag"></i>
                                        <span><?php echo "". sizeof($_SESSION['cart']); ?></span>
                                    </div>
                                    <a href="./cart.php">Shopping Cart</a>
                                </div>
                            </div>
                        </div>
                    
                </div>
            </div>
        </div>
        <nav class="main-navbar">
            <div class="container">
                <!-- menu -->
                <ul class="main-menu">
                    <li><a href="./index.view.php">Home</a></li>        
                    <li><a href="./vetPage.php">Veterinary Services</a>
                        <ul class="sub-menu">
                            <li><a href="#">Checkup and Consultation</a></li>
                            <!-- <li><a href="./petVitandSupplementsPage.php">Vitamins and Supplements</a></li> -->
                        </ul>
                    </li>                   
            

                    
                
                    <li><a href="#">Pet Shop</a>
                        <ul class="sub-menu">
                            <li><a href="./petFashionPage.php">Pet fashion and accessories</a></li>
                            <li><a href="./petFoodPage.php">Pet Food</a></li>
                             
                        </ul>
                    </li>
                    
                    <li><a href="#">Pages</a>
                        <ul class="sub-menu">
                            <li><a href="./petFoodPage.php">Product Page</a></li>
                            <li><a href=" ">Category Page</a></li>
                            <li><a href="./cart.php">Cart Page</a></li>
                            <li><a href="./checkout.php">Checkout Page</a></li>
                            
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Header section end -->

    <!-- Product filter section -->
<section class="product-filter-section">
    <div class="container">
        <div class="section-title"><br>
           <h1>Schedule Your Pet Checkup</h1>

<form action="./add_schedule.php" method="POST">
  Input Date: <input type="date" name="sched">
  Input Time: <input type="Time" name="timez">

  <input type="submit"  >


</form>

<hr><br>
<h3>Scheduled Reservations:</h3>

<table style="width: 100%;">
    <thead>
        <tr>
            <th>Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Action</th>

        </tr>

    </thead>
    <tbody>
        <?php foreach ($sched as $scheds)   : ?>
        <tr>
            
               
               

                <td><?= $scheds->name; ?></td>
                <td><?= $scheds->datez; ?></td>
                <td><?= $scheds->timez; ?></td>
                <td><a href='./remove_schedule.php?id=<?=$scheds->id?>'>Remove</a></td>

               

            

        </tr>
 <?php endforeach; ?> 

    </tbody>


</table>




<!-- <p><strong>Note:</strong> type="date" is not supported in Safari or Internet Explorer 11 (or earlier).</p> -->

<script src="../../js/jquery-3.2.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/jquery.slicknav.min.js"></script>
    <script src="../../js/owl.carousel.min.js"></script>
    <script src="../../js/jquery.nicescroll.min.js"></script>
    <script src="../../js/jquery.zoom.min.js"></script>
    <script src="../../js/jquery-ui.min.js"></script>
    <script src="../../js/main.js"></script>

           <!--  <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <div class="tag-sale">ON SALE</div>
                        <img src="../../img/product/pet foods/alpoDogFood.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>₱35.00</h6>
                        <p>Alpo dog food</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="./img/product/pet foods/iams.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>₱35.00</h6>
                        <p>IAMS </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="./img/product/pet foods/seraNatureFishFood.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>₱35.00</h6>
                        <p>Sera Nature Fish Food </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="product-item">
                    <div class="pi-pic">
                        <img src="./img/product/pet foods/whiskas.jpg" alt="">
                        <div class="pi-links">
                            <a href="#" class="add-card"><i class="flaticon-bag"></i><span>ADD TO CART</span></a>
                            <a href="#" class="wishlist-btn"><i class="flaticon-heart"></i></a>
                        </div>
                    </div>
                    <div class="pi-text">
                        <h6>₱35.00</h6>
                        <p>Whiskas </p>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="text-center pt-5">
            <button class="site-btn sb-line sb-dark">LOAD MORE</button>
        </div>
    </div> -->
</section>
<!-- Product filter section end -->


    

    <!-- Footer section -->
    <section class="footer-section">
        <div class="container">
            <div class="footer-logo text-center">
                <a href="index.view.php"><img src="./img/pethealth.banner.png" alt=""></a>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget about-widget">
                        <h2>About</h2>
                        <p>Donec vitae purus nunc. Morbi faucibus erat sit amet congue mattis. Nullam frin-gilla faucibus urna, id dapibus erat iaculis ut. Integer ac sem.</p>
                        <img src="img/cards.png" alt="">
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget about-widget">
                        <h2>Questions</h2>
                        <ul>
                            <li><a href="./aboutUs.php">About Us</a></li>
                            <li><a href="">Track Orders</a></li>
                            
                            
                            <li><a href="">Shipping</a></li>
                            
                        </ul>
                        <ul>
                            <li><a href="">Partners</a></li>
                            <li><a href="">Bloggers</a></li>
                        
                            <li><a href="">Terms of Use</a></li>
                            
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget about-widget">
                        <h2>Questions</h2>
                        <div class="fw-latest-post-widget">
                            <div class="lp-item">
                                <div class="lp-thumb set-bg" data-setbg="img/blog-thumbs/sickpet.jpg"></div>
                                <div class="lp-content">
                                    <h6>Signs that my pet is sick</h6>
                                    
                                    <a href="https://pets.webmd.com/features/pet-symptoms-6-signs-illness-dog-cat" class="readmore">Read More</a>
                                </div>
                            </div>
                            <div class="lp-item">
                                <div class="lp-thumb set-bg" data-setbg="img/blog-thumbs/cats.png"></div>
                                <div class="lp-content">
                                    <h6>The Way of the Cats</h6>
                                    
                                    <a href="http://www.wayofcats.com/blog/" class="readmore">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="footer-widget contact-widget">
                        <h2>Questions</h2>
                        <div class="con-info">
                            <span>C.</span>
                            <p>PetHealth.com Ltd </p>
                        </div>
                        <div class="con-info">
                            <span>B.</span>
                            <p>Baguio City, Philippines </p>
                        </div>
                        <div class="con-info">
                            <span>T.</span>
                            <p>+63 921 123 4567</p>
                        </div>
                        <div class="con-info">
                            <span>E.</span>
                            <p>petHealth.com@petHealth.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="social-links-warp">
            <div class="container">         
            </div>
        </div>
        
    </section>
    <!-- Footer section end -->



    <!--====== Javascripts & Jquery ======-->
    <script src="../../js/jquery-3.2.1.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/jquery.slicknav.min.js"></script>
    <script src="../../js/owl.carousel.min.js"></script>
    <script src="../../js/jquery.nicescroll.min.js"></script>
    <script src="../../js/jquery.zoom.min.js"></script>
    <script src="../../js/jquery-ui.min.js"></script>
    <script src="../../js/main.js"></script>

    </body>
</html>


 