<?php session_start();  

	
?>

<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>PetHealth.com | WIS1</title>
	<meta charset="UTF-8">
	<meta name="description" content=" PetHealth.com | Technopreneurship">
	<meta name="keywords" content="PetHealth.com, eCommerce, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">


	<!-- Stylesheets -->
	<link rel="stylesheet" href="../../css/bootstrap.min.css"/>
	<link rel="stylesheet" href="../../css/font-awesome.min.css"/>
	<link rel="stylesheet" href="../../css/flaticon.css"/>
	<link rel="stylesheet" href="../../css/slicknav.min.css"/>
	<link rel="stylesheet" href="../../css/jquery-ui.min.css"/>
	<link rel="stylesheet" href="../../css/owl.carousel.min.css"/>
	<link rel="stylesheet" href="../../css/animate.css"/>
	<link rel="stylesheet" href="../../css/style.css"/>


	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<header class="header-section">
		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 text-center text-lg-left">
						<!-- logo -->
						<a href="../../views/dashboard/index.view.php" class="site-logo">
							<img src="../../img/logo.png" alt="">
						</a>
					</div>
					<div class="col-xl-6 col-lg-5">
						<form class="header-search-form">
							<input type="text" placeholder="Search on Pethealth.com ....">
							<button><i class="flaticon-search"></i></button>
						</form>
					</div>
					<div class="col-xl-4 col-lg-5">
							<div class="user-panel">
								<div class="up-item">
									<i class="flaticon-profile"></i>
									<a href="#"><?php echo $_SESSION['username']; ?></a>
								</div>
								<div class="up-item">
									<div class="shopping-card">
										<i class="flaticon-bag"></i>
										<span><?php echo "". sizeof($_SESSION['cart']); ?></span>
									</div>
									<a href="../../views/dashboard/cart.php">Shopping Carts</a>
								</div>
							</div>
						</div>
					
				</div>
			</div>
		</div>
		<nav class="main-navbar">
			<div class="container">
				<!-- menu -->
				<ul class="main-menu">
					<li><a href="#">Home</a></li>		
					<li><a href="#">Vetenirary Services</a>
						<ul class="sub-menu">
							<li><a href="../../views/dashboard/Petreserve.php">Checkup and Consultation</a></li>
						</ul>
					</li>					
					
				
					<li><a href="#">Pet Shop</a>
						<ul class="sub-menu">
							<li><a href="./petFashionPage.php">Pet fashion and accessories</a></li>
							<li><a href="./petFoodPage.php">Pet Food</a></li>
							
						</ul>
					</li>
					
					<li><a href="#">Pages</a>
						<ul class="sub-menu">
							<li><a href="../../views/dashboard/petFoodPage.php">Product Page</a></li>
							<li><a href=" ">Category Page</a></li>
							<li><a href="./cart.php">Cart Page</a></li>
							<li><a href="./checkout.php">Checkout Page</a></li>
							
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- Header section end -->



	<!-- Hero section -->
	<section class="hero-section">
		<div class="hero-slider owl-carousel">
			<div class="hs-item set-bg" data-setbg="../../img/j.jpg">
				<div class="container">
					<div class="row">
						<div class="col-xl-6 col-lg-7 text-black">
							<span>Pet  of the day! </span>
							<h2>JiffPom</h2>
							<p>Record-breaker for an animal with the most Instagram followers, earns $5,950,000 a year. That’s a whole lot of dog treats!
								 </p>
							
						</div>
					</div>
				
				</div>
			</div>
			<div class="hs-item set-bg" data-setbg="../../img/jiffpom2.jpg">
				<div class="container">
					<div class="row">
						<div class="col-xl-6 col-lg-7 text-black">
							<span>Pet of the day!</span>
							<h2>JiffPom</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum sus-pendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<div class="container">
			<div class="slide-num-holder" id="snh-1"></div>
		</div>
	</section>
	<!-- Hero section end -->



	<!-- Features section -->
	<section class="features-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4 p-0 feature">
					<div class="feature-inner">
						<div class="feature-icon">
							<img src="../../img/icons/vet logo.png" alt="#">
						</div>
						<h2>Need a Vet?</h2>
					</div>
				</div>
				<div class="col-md-4 p-0 feature">
					<div class="feature-inner">
						<div class="feature-icon">
							<img src="../../img/icons/pet prod logo.jpg" alt="#">
						</div>
						<h2>Looking for Pet Products? </h2>
					</div>
				</div>
				<div class="col-md-4 p-0 feature">
					<div class="feature-inner">
						<div class="feature-icon">
							<img src="../../img/icons/pet grooming.png" alt="#">
						</div>
						<h2>Searching for pet services?</h2>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Features section end -->



	<!-- Footer section -->
	<section class="footer-section">
		<div class="container">
			<div class="footer-logo text-center">
				<a href="index.view.php"><img src="../../img/pethealth.banner.png" alt=""></a>
			</div>
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget about-widget">
						<h2>About</h2>
						<p>PetHealth.com is a website that provides access to different pet related activities and products.</p>
						<img src="../../img/cards.png" alt="">
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget about-widget">
						<h2>Questions</h2>
						<ul>
							<li><a href="./aboutUs.php">About Us</a></li>
							<li><a href="">Track Orders</a></li>
							
							
							<li><a href="">Shipping</a></li>
							
						</ul>
						<ul>
							<li><a href="">Partners</a></li>
							<li><a href="">Blogs</a></li>
						
							<li><a href="">Terms of Use</a></li>
							
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget about-widget">
						<h2>Questions</h2>
						<div class="fw-latest-post-widget">
							<div class="lp-item">
								<div class="lp-thumb set-bg" data-setbg="img/blog-thumbs/sickpet.jpg"></div>
								<div class="lp-content">
									<h6>Signs that my pet is sick</h6>
									
									<a href="https://pets.webmd.com/features/pet-symptoms-6-signs-illness-dog-cat" class="readmore">Read More</a>
								</div>
							</div>
							<div class="lp-item">
								<div class="lp-thumb set-bg" data-setbg="img/blog-thumbs/cats.png"></div>
								<div class="lp-content">
									<h6>The Way of the Cats</h6>
									
									<a href="http://www.wayofcats.com/blog/" class="readmore">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget contact-widget">
						<h2>Questions</h2>
						<div class="con-info">
							<span>C.</span>
							<p>PetHealth.com Ltd </p>
						</div>
						<div class="con-info">
							<span>B.</span>
							<p>Baguio City, Philippines </p>
						</div>
						<div class="con-info">
							<span>T.</span>
							<p>+63 921 123 4567</p>
						</div>
						<div class="con-info">
							<span>E.</span>
							<p>petHealth.com@petHealth.com</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="social-links-warp">
			<div class="container">
			
				</div>

				



			</div>
		</div>
	</section>
	<!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
	<script src="../../js/jquery-3.2.1.min.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<script src="../../js/jquery.slicknav.min.js"></script>
	<script src="../../js/owl.carousel.min.js"></script>
	<script src="../../js/jquery.nicescroll.min.js"></script>
	<script src="../../js/jquery.zoom.min.js"></script>
	<script src="../../js/jquery-ui.min.js"></script>
	<script src="../../js/main.js"></script>

	</body>
</html>
