<?php 


// $a = [1,2,3];
// $b = 0; 

//  foreach ($a as $key) {
//  	# code...
//  	$b += $key;
//  }
 
// echo "res ".$b;

 

 
// die();

 session_start(); 
 if(!isset($_SESSION['cart'])) {
 	$_SESSION['cart'] = [];

 }

 ?>

<!DOCTYPE html>
<html lang="zxx">
	<head>
		<title>PetHealth.com | Wis1</title>
		<meta charset="UTF-8">
		<meta name="description" content=" PetHealth.com ">
		<meta name="keywords" content="PetHealth.com, eCommerce, html">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Favicon -->
		<link href="img/favicon.ico" rel="shortcut icon"/>
	
		<!-- Google Font -->
		<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">
	
	
		<!-- Stylesheets -->
		<link rel="stylesheet" href="../../css/bootstrap.min.css"/>
		<link rel="stylesheet" href="../../css/font-awesome.min.css"/>
		<link rel="stylesheet" href="../../css/flaticon.css"/>
		<link rel="stylesheet" href="../../css/slicknav.min.css"/>
		<link rel="stylesheet" href="../../css/jquery-ui.min.css"/>
		<link rel="stylesheet" href="../../css/owl.carousel.min.css"/>
		<link rel="stylesheet" href="../../css/animate.css"/>
		<link rel="stylesheet" href="../../css/style.css"/>
	
	
		<!--[if lt IE 9]>
			  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	<style type="text/css">
		
		td{

			text-align: center;

		}

	</style>

	</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<header class="header-section">
		<div class="header-top">
			<div class="container">
				<div class="row">
					<div class="col-lg-2 text-center text-lg-left">
						<!-- logo -->
						<a href="./index.php" class="site-logo">
							<img src="img/logo.png" alt="">
						</a>
					</div>
					<div class="col-xl-6 col-lg-5">
						<form class="header-search-form">
							<input type="text" placeholder="Search on Pethealth.com ....">
							<button><i class="flaticon-search"></i></button>
						</form>
					</div>
					<div class="col-xl-4 col-lg-5">
						<div class="user-panel">
							<div class="up-item">
								<i class="flaticon-profile"></i>
								<a href="#"><?php echo $_SESSION['username'] ?> 
							</div>
							<div class="up-item">
								<div class="shopping-card">
									<i class="flaticon-bag"></i>
									<span><?php echo "". sizeof($_SESSION['cart']); ?></span>
								</div>
								<a href="#">Shopping Cart</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="main-navbar">
			<div class="container">
				<!-- menu -->
				<ul class="main-menu">
					<li><a href="./index.view.php">Home</a></li>		
					<li><a href="./vetPage.php">Vetenirary Services</a>
						<ul class="sub-menu">
							<li><a href="./Petreserve.php">Checkup and Consultation</a></li>
							<!-- <li><a href="./petVitandSupplementsPage.php">Vitamins and Supplements</a></li> -->
						</ul>
					</li>					
					<li><a href="./petServices.php">Pet Services</a>
						<ul class="sub-menu">
							<li><a href="./petBreedingPage.php">Breeding</a></li>
							<li><a href="./petGroomingPage.php">Grooming</a></li>
						</ul>
					</li>

					<li><a href="#">Own a Pet</a>
						<ul class="sub-menu">
							<li><a href="#">Buy and Sell</a></li>							
							<li><a href="#">Adopt</a></li>
						</ul>
					</li>
					
				
					<li><a href="#">Pet Shop</a>
						<ul class="sub-menu">
							<li><a href="./petFashionPage.php">Pet fashion and accessories</a></li>
							<li><a href="./petFoodPage.php">Pet Food</a></li>
							
						</ul>
					</li>
					<li><a href="#">Pet Day-care</a>
						<ul class="sub-menu">
							<li><a href="#">Pet Training</a></li>
							<li><a href="#">Pet Babysitting</a></li>						
						</ul>
					</li>
					<li><a href="#">Pages</a>
						<ul class="sub-menu">
							<li><a href="./petFoodPage.php">Product Page</a></li>
							<li><a href=" ">Category Page</a></li>
							<li><a href="./cart.php">Cart Page</a></li>
							<li><a href="./checkout.php">Checkout Page</a></li>
							
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<!-- Header section end -->


	<!-- Page info -->
	<div class="page-top-info">
		<div class="container">
			<h4>Your cart</h4>
			<div class="site-pagination">
				<a href="./index.php"> Home </a> /
				<a href="./cart.php">Your cart</a>
			</div>
		</div>
	</div>
	<!-- Page info end -->

	<!-- cart section end -->
	<section class="cart-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="cart-table">
						<h3>Your Cart</h3>
						<div class="cart-table-warp">
							<table>
							<thead>
								<tr>
									
									<!-- <th class="product-th">Product</th> -->
									<!-- <th class="quy-th">Quantity</th> -->
									
									<!-- <th class="total-th">Price</th> -->
									<!-- <th class="total-th">Actions</th> -->
									<th>Product</th>
									<th>Quantity</th>
									<th>Price</th>
									<th>Actions</th>

								</tr>
							</thead>
							<tbody>

								<?php  foreach($_SESSION['cart'] as $cart_item):?>
								 
								

								<tr >
									<td >  <?=$cart_item->name ?></td>
									<td> 1</td>
									<td> <?=$cart_item->price?></td>
									<td><a href="./remove_item_cart.php?id=<?=$cart_item->id?>"> remove</a></td> 


							<?php endforeach; ?>





									<!-- <td class="product-col" > -->
										<!-- <img src="#" alt=""> -->
										<!-- ../../img/cart/1.jpg -->
										<!-- <div class="pc-title"> -->
											<!-- <h4><?=$cart_item->name ?></h4> -->
											<!-- <h4 ">hi</h4> -->
											<!-- <p>$45.90</p> -->
										<!-- </div> -->
									<!-- </td> -->



									<!-- <td class="quy-col"> -->
										<!-- <div class="quantity"> -->
					                        <!-- <div class="pro-qty"> -->
												<!-- <input type="text" value="1"> -->
											<!-- </div> -->
                    					<!-- </div> -->
									<!-- </td> -->

									
									
									<!-- <td class="total-col"><h4>$45.90</h4></td> -->
								</tr>

								




<!-- 
								<tr>
									<td class="product-col">
										<img src="img/cart/2.jpg" alt="">
										<div class="pc-title">
											<h4>Pedigree</h4>
											<p>$45.90</p>
										</div>
									</td>
									<td class="quy-col">
										<div class="quantity">
					                        <div class="pro-qty">
												<input type="text" value="1">
											</div>
                    					</div>
									</td>
									
									<td class="total-col"><h4>$45.90</h4></td>
								</tr>
								<tr>
									<td class="product-col">
										<img src="img/cart/3.jpg" alt="">
										<div class="pc-title">
											<h4>Whiskas</h4>
											<p>$45.90</p>
										</div>
									</td>
									<td class="quy-col">
										<div class="quantity">
					                        <div class="pro-qty">
												<input type="text" value="1">
											</div>
                    					</div>
									</td>
									
									<td class="total-col"><h4>$45.90</h4></td>
								</tr> -->
							</tbody>
						</table>
						</div> 

						<div class="total-cost">
								<?php $res = 0; ?>
								<?php  foreach($_SESSION['cart'] as $cart_item):?>
								<?php $res += $cart_item->price ?>


								<?php endforeach; ?>
							<h6>Total <span>$ <?php echo " ".$res; ?></span></h6>
						</div>
					</div>
				</div>
				<div class="col-lg-4 card-right">					
					<a href="./checkout.php" class="site-btn">Proceed to checkout</a>
					<a href="./petFoodPage.php" class="site-btn sb-dark">Continue shopping</a>
				</div>
			</div>
		</div>
	</section>
	<!-- cart section end -->

	



	<!-- Footer section -->
	<section class="footer-section">
		<div class="container">
			<div class="footer-logo text-center">
				<a href="index.php"><img src="./img/pethealth.banner.png" alt=""></a>
			</div>
			<div class="row">
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget about-widget">
						<h2>About</h2>
						<p>Donec vitae purus nunc. Morbi faucibus erat sit amet congue mattis. Nullam frin-gilla faucibus urna, id dapibus erat iaculis ut. Integer ac sem.</p>
						<img src="img/cards.png" alt="">
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget about-widget">
						<h2>Questions</h2>
						<ul>
							<li><a href="./aboutUs.php">About Us</a></li>
							<li><a href="">Track Orders</a></li>
							
							
							<li><a href="">Shipping</a></li>
							
						</ul>
						<ul>
							<li><a href="">Partners</a></li>
							<li><a href="">Bloggers</a></li>
						
							<li><a href="">Terms of Use</a></li>
							
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget about-widget">
						<h2>Questions</h2>
						<div class="fw-latest-post-widget">
							<div class="lp-item">
								<div class="lp-thumb set-bg" data-setbg="img/blog-thumbs/sickpet.jpg"></div>
								<div class="lp-content">
									<h6>Signs that my pet is sick</h6>
									
									<a href="https://pets.webmd.com/features/pet-symptoms-6-signs-illness-dog-cat" class="readmore">Read More</a>
								</div>
							</div>
							<div class="lp-item">
								<div class="lp-thumb set-bg" data-setbg="img/blog-thumbs/cats.png"></div>
								<div class="lp-content">
									<h6>The Way of the Cats</h6>
									
									<a href="http://www.wayofcats.com/blog/" class="readmore">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-sm-6">
					<div class="footer-widget contact-widget">
						<h2>Questions</h2>
						<div class="con-info">
							<span>C.</span>
							<p>PetHealth.com Ltd </p>
						</div>
						<div class="con-info">
							<span>B.</span>
							<p>Baguio City, Philippines </p>
						</div>
						<div class="con-info">
							<span>T.</span>
							<p>+63 921 123 4567</p>
						</div>
						<div class="con-info">
							<span>E.</span>
							<p>petHealth.com@petHealth.com</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="social-links-warp">
			<div class="container">
			
				</div>

				



			</div>
		</div>
	</section>
	<!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
	<script src="../../js/jquery-3.2.1.min.js"></script>
	<script src="../../js/bootstrap.min.js"></script>
	<script src="../../js/jquery.slicknav.min.js"></script>
	<script src="../../js/owl.carousel.min.js"></script>
	<script src="../../js/jquery.nicescroll.min.js"></script>
	<script src="../../js/jquery.zoom.min.js"></script>
	<script src="../../js/jquery-ui.min.js"></script>
	<script src="../../js/main.js"></script>

	</body>
</html>
