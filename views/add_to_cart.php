<?php
	session_start();
	require('../core/functions.php');
	//connect to db
	$pdo = connectToDB();
	//prepare stmt
	$statement = $pdo->prepare('SELECT * FROM products WHERE id = :id');
	//replace id with the actual id
	$statement->execute([
		'id' => $_GET['id']
	]);
	//fetch single product record
	$product = $statement->fetch(PDO::FETCH_OBJ);
	//check if cart exists
	if(!isset($_SESSION['cart'])) {
		//if no then create cart
		$_SESSION['cart'] = [];
	}
	//if yes then push to array
	$_SESSION['cart'][] = $product;

	// var_dump($_SESSION['cart']);

	
	header('Location: ./dashboard/petFoodPage.php');



	exit();

?>